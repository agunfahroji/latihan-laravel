<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function daftar() {
        return view('halaman.daftar');
    }

    public function kirim(Request $request) {
        $firstname = $request['firstname'];
        $lastname = $request['lastname'];

        return view('halaman.welcome', compact('firstname', 'lastname'));
    }
}
