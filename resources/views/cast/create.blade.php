@extends('layout.master')
@section('judul')
<h1>Tambah Cast</h1>
@endsection

@section('isi') 
    <form action="/cast" method="POST">
        @csrf
        <div class="form-group">
        <label for="exampleInputEmail1">Nama Cast</label>
        <input type="text" name="nama" class="form-control">
        </div>
        @error('nama')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
        <label >Umur</label>
        <input type="text" name="umur" class="form-control">
        </div>
        @error('umur')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
        <label >bio</label>
        <textarea name="bio" id="" cols="30" rows="10" class="form-control"></textarea>
        </div>
        @error('bio')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <button type="submit" class="btn btn-primary">Tambah</button>
    </form>
    
@endsection