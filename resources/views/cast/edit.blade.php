@extends('layout.master')
@section('judul')
<h1>Edit Cast {{$cast->nama}}</h1>
@endsection

@section('isi') 
    <form action="/cast/{{$cast->id}}" method="POST">
        @csrf
        @method('put')
        <div class="form-group">
        <label for="exampleInputEmail1">Nama Cast</label>
        <input type="text" name="nama" value="{{$cast->nama}}" class="form-control">
        </div>
        @error('nama')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
        <label >Umur</label>
        <input type="text" name="umur" value="{{$cast->umur}}" class="form-control">
        </div>
        @error('umur')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
        <label >bio</label>
        <textarea name="bio" id="" cols="30" rows="10" class="form-control">{{$cast->bio}}</textarea>
        </div>
        @error('bio')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <button type="submit" class="btn btn-primary">Update</button>
    </form>
    
@endsection