@extends('layout.master')
@section('judul')
<h1>Detail Cast {{$cast->nama}}</h1>
@endsection

@section('isi') 
    <h1>Nama : {{$cast->nama}}</h1>
    <p>Umur : {{$cast->umur}}</p>
    <p>Bio : {{$cast->bio}}</p>
    
    <a href="/cast" class="btn btn-secondary">Kembali</a>
@endsection 