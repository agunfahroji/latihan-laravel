@extends('layout.master')
@section('judul')
<h1>Buat Account Baru!</h1>
@endsection

@section('isi') 
    <h2>Sign Up Form</h2>
    <form action="{{url('/kirim')}}" method="post">
        @csrf
        <label for="firstname">First name :</label>
        <br><br>
        <input type="text" name="firstname" id="firstname" required>
        <br><br>
        <label for="lastname">Last name :</label>
        <br><br>
        <input type="text" name="lastname" id="lastname" required>
        <br><br>
        <label for="gender">Gender :</label>
        <br><br>
        <input type="radio" id="male" name="gender"><label for="male">Male</label>
        <input type="radio" id="female" name="gender"><label for="male">Female</label>
        <input type="radio" id="other" name="gender"><label for="other">Other</label>
        <br><br>
        <label for="nationality">Nationality :</label>
        <br><br>
        <select name="nationality" id="nationality">
            <option value="1">Indonesia</option>
            <option value="2">Singapura</option>
            <option value="3">Malaysia</option>
            <option value="4">Australia</option>
        </select>
        <br><br>
        <label for="language">Language Spoken :</label>
        <br><br>
        <input type="checkbox" id="indonesia"><label for="indonesia">Bahasa Indonesia</label>
        <input type="checkbox" id="englis"><label for="englis">Englis</label>
        <input type="checkbox" id="other"><label for="other">Other</label>
        <br><br>
        <label for="bio">Bio :</label>
        <br><br>
        <textarea name="bio" id="" cols="30" rows="10"></textarea>
        <br><br>
        <button type="submit">Sign Up</button>
    </form>
@endsection